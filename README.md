# BL3-Kassiopeia

Geometry and analysis files to BL3 trap implementation in Kassiopeia

## Running simulations with installed Kassiopeia

Install Kassiopeia, create a user directory (e.g. git clone this repository will do), ensure that your kasperenv.sh is loaded. Then the simulation should run with the following command:
```
Kassiopeia ProtonTrapSimulation.xml
```
If VTK is installed, a window will pop up with the simulated tracks.

## Running simulations with Kassiopeia container

Clone this repository to get the latest xml files:
```
git clone https://gitlab.com/BL3/bl3-kassiopeia.git
cd bl3-kassiopeia
```
Use singularity to run Kassiopeia on these files
```
singularity run docker://wdconinc/kassiopeia create_kasper_user_directory.sh .
singularity run docker://wdconinc/kassiopeia Kassiopeia ProtonTrapSimulation.xml
```

## Running simulations on Slurm HPC clusters

Clone this repository to get the latest xml files:
```
git clone https://gitlab.com/BL3/bl3-kassiopeia.git
cd bl3-kassiopeia
```
Use singularity to get a static Kassiopeia container:
```
singularity pull docker://wdconinc/kassiopeia
```
Submit the job file with sbatch:
```
sbatch job.sh
```
Note: Look in job.sh and adapt paths as necessary.
