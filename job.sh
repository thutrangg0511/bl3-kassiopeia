#!/bin/bash
#SBATCH --account=def-wdconinc
#SBATCH --job-name=ProtonTrapSimulation
#SBATCH --mail-type=END,FAIL,REQUEUE
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=2gb
#SBATCH --time=24:00:00
#SBATCH --output=ProtonTrapSimulation_%A-%a.out
#SBATCH --array=1-10

DIR=$HOME/scratch/${SLURM_JOBID}.${SLURM_ARRAY_TASK_ID}
mkdir -p $DIR && cd $DIR
echo "Current working directory is `pwd`"

RUN=$HOME/git/bl3-kassiopeia/kassiopeia_latest.sif
if [ ! -f ${RUN} -a ! -x ${RUN} ] ; then
  echo "Could not find or execute ${RUN}"
  echo "Run 'singularity pull docker://wdconinc/kassiopeia:latest' first"
  exit
fi

echo "Starting job ${SLURM_JOBID} run ${SLURM_ARRAY_TASK_ID} at: `date`"
DIR=$HOME/git/bl3-kassiopeia
XML=ProtonTrapSimulation.xml
cp -r ${DIR}/*.xml .
${RUN} create_kasper_user_directory.sh `pwd`
${RUN} Kassiopeia ${XML} -r run=${SLURM_ARRAY_TASK_ID} seed=${SLURM_ARRAY_TASK_ID} events=10 graphics=0

sstat ${SLURM_JOBID}
echo "Job finished with exit code $? at: `date`"
