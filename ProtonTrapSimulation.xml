<external_define name="run" value="1"/>
<external_define name="seed" value="51386"/>
<external_define name="events" value="100"/>
<external_define name="graphics" value="1"/>

<global_define name="output_path" value="[KASPERSYS]/output/Kassiopeia"/>
<global_define name="log_path" value="[KASPERSYS]/log/Kassiopeia"/>

<global_define name="electrode_fillet" value="0.e-3"/>

<include name="messages.xml"/>

<geometry>

    <include name="geometry.xml"/>

    <!-- electrostatic -->

    <include name="electrostatic_closed_trap.xml"/>
    <include name="electrostatic_detector.xml"/>

    <!-- electromagnets -->
    <include name="electromagnet_currents.xml"/>

</geometry>
    
<kassiopeia>

    <!-- magnetic fields -->
    <include name="field_electromagnet.xml"/>

    <!-- electric fields -->
    <include name="field_electrostatic.xml"/>

    <!-- generators -->

    <ksgen_generator_composite name="generator_neutron_beta_decay" pid="2212">
        <energy_beta_recoil name="beta_recoil"/>
        <position_cylindrical_composite surface="world/proton_trap/beam">
            <!-- "The neutron beam diameter will increase from 7 mm to 30 mm." NSF 2020 Project Description -->
            <r_cylindrical radius_min="0." radius_max="3.0e-2"/>
            <phi_uniform value_min="0." value_max="360."/>
            <z_uniform value_min="-25.e-2" value_max="+25.e-2"/>
        </position_cylindrical_composite>
        <direction_spherical_composite surface="world/proton_trap/beam">
            <theta_uniform value_min="0." value_max="180."/>
            <phi_uniform value_min="0." value_max="360"/>
        </direction_spherical_composite>
        <time_composite>
            <time_fix value="0."/>
        </time_composite>
    </ksgen_generator_composite>

    <ksgen_generator_composite name="generator_equilibrium_distribution" pid="2212">
        <position_rectangular_composite surface="world/proton_trap/beam">
            <x_histogram name="position_x_histogram" path="[output_path]" base="ProtonTrapEquilibrium.root" histogram="final_position_x"/>
            <y_histogram name="position_y_histogram" path="[output_path]" base="ProtonTrapEquilibrium.root" histogram="final_position_y"/>
            <z_histogram name="position_z_histogram" path="[output_path]" base="ProtonTrapEquilibrium.root" histogram="final_position_z"/>
        </position_rectangular_composite>
        <momentum_rectangular_composite surface="world/proton_trap/beam">
            <x_histogram name="momentum_x_histogram" path="[output_path]" base="ProtonTrapEquilibrium.root" histogram="final_momentum_x"/>
            <y_histogram name="momentum_y_histogram" path="[output_path]" base="ProtonTrapEquilibrium.root" histogram="final_momentum_y"/>
            <z_histogram name="momentum_z_histogram" path="[output_path]" base="ProtonTrapEquilibrium.root" histogram="final_momentum_z"/>
        </momentum_rectangular_composite>
        <time_composite>
            <time_fix value="0."/>
        </time_composite>
    </ksgen_generator_composite>

    <ksgen_generator_composite name="generator_bounce_back" pid="2212">
        <energy_composite>
            <energy_fix value="25.e3"/>
        </energy_composite>
        <position_cylindrical_composite surface="world/proton_trap/proton_target">
            <r_cylindrical radius_min="0." radius_max="6.0e-2"/>
            <phi_uniform value_min="0." value_max="360."/>
            <z_uniform value_min="0." value_max="0."/>
        </position_cylindrical_composite>
        <direction_spherical_composite surface="world/proton_trap/proton_target">
            <theta_uniform value_min="90." value_max="180."/>
            <phi_uniform value_min="0." value_max="360"/>
        </direction_spherical_composite>
        <time_composite>
            <time_fix value="0."/>
        </time_composite>
    </ksgen_generator_composite>

    <!-- trajectories-->

    <kstraj_trajectory_exact name="trajectory_fast">
        <interpolator_fast name="interpolator_fast"/>
        <integrator_rk54 name="integrator_rk54"/>
        <term_propagation name="term_propagation"/>
        <control_position_error name="control_position_error" absolute_position_error="1e-6" safety_factor="0.75" solver_order="5"/>
    </kstraj_trajectory_exact>

    <kstraj_trajectory_exact name="trajectory_exact">
        <interpolator_crk name="interpolator_crk"/>
        <integrator_rkdp853 name="integrator_rkdp853"/>
        <term_propagation name="term_propagation"/>
        <control_position_error name="control_position_error" absolute_position_error="1e-12" safety_factor="0.75" solver_order="8"/>
        <control_cyclotron name="control_cyclotron_1_64" fraction="{1. / 64.}"/>
    </kstraj_trajectory_exact>

    <!-- space navigators -->

    <ksnav_space name="nav_space" enter_split="false" exit_split="false"/>
    <ksnav_meshed_space name="nav_meshed_space" root_space="space_world" max_octree_depth="9" n_allowed_elements="1"/>

    <!-- surface navigators -->

    <ksnav_surface name="nav_surface" transmission_split="false" reflection_split="false"/>

    <!-- surface interactions -->

    <ksint_surface_diffuse name="int_surface_diffuse" probability=".01" reflection_loss="0." transmission_loss="1."/>

    <!-- terminators -->

    <ksterm_death name="term_death"/>
    <ksterm_death name="term_world"/>
    <ksterm_death name="term_proton_target"/>
    <ksterm_min_z name="term_min_z" z="0.0"/>
    <ksterm_max_z name="term_max_z" z="1.5"/>
    <ksterm_max_r name="term_max_radius" r="1.0" />
    <ksterm_max_steps name="term_max_steps" steps="200000"/>
    <ksterm_min_energy name="term_min_energy" energy=".01"/>
    <!-- "Periodically, typically every 10 ms, the three “door” electrodes are lowered to ground." NSF 2020 Project Description -->
    <ksterm_max_step_time name="term_max_step_time" time="0.010"/>

    <!-- writers -->

    <kswrite_root name="write_root" path="[output_path]" base="ProtonTrapSimulation.root"/>
    <kswrite_vtk name="write_vtk" path="[output_path]" base="ProtonTrapSimulation"/>

    <!-- output -->

    <include name="output_track.xml"/>
    <include name="output_step.xml"/>

    <!-- structure -->

    <ksgeo_space name="space_world" spaces="world">
        <command parent="root_terminator" field="remove_terminator" child="term_world"/>
        <command parent="root_terminator" field="add_terminator" child="term_max_steps"/>
        <command parent="root_terminator" field="add_terminator" child="term_max_z"/>
        <command parent="root_terminator" field="add_terminator" child="term_min_z"/>
        <command parent="root_terminator" field="add_terminator" child="term_max_radius"/>
        <command parent="root_terminator" field="add_terminator" child="term_min_energy"/>
        <command parent="write_root" field="add_track_output" child="component_track_world"/>
        <command parent="write_root" field="add_step_output" child="component_step_world"/>
        <!--command parent="write_vtk" field="set_track_point" child="component_track_position"/-->
        <!--command parent="write_vtk" field="set_track_data" child="component_track_length"/-->
        <command parent="write_vtk" field="set_step_point" child="component_step_position"/>
        <command parent="write_vtk" field="set_step_data" child="component_step_length"/>
        <geo_surface name="surface_proton_target" surfaces="world/proton_trap/proton_target">
            <command parent="root_surface_interaction" field="set_surface_interaction" child="int_surface_diffuse"/>
            <command parent="root_terminator" field="add_terminator" child="term_proton_target"/>
        </geo_surface>
        <geo_surface name="world_boundary_surface" surfaces="world/world_boundary">
            <command parent="root_terminator" field="add_terminator" child="term_death"/>
        </geo_surface>
    </ksgeo_space>

    <!-- simulation -->

    <ks_simulation
        run="[run]"
        seed="[seed]"
        events="[events]"
        magnetic_field="magnetic_field_electromagnet"
        electric_field="electric_field_electrostatic"
        space="space_world"
        generator="generator_neutron_beta_decay"
        trajectory="trajectory_fast"
        space_navigator="nav_space"
        surface_navigator="nav_surface"
        terminator="term_world"
        writer="write_root"
        writer="write_vtk"
    />

</kassiopeia>

<if condition="{[graphics] gt 0}">
    <include name="window_vtk.xml"/>
    <include name="window_root_zx.xml"/>
    <include name="window_root_xy.xml"/>
</if>
